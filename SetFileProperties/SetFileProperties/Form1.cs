﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace SetFileProperties
{
    public partial class FormMain : Form
    {
        BackgroundWorker w = new BackgroundWorker();

        public FormMain()
        {
            InitializeComponent();
        }

        private void buttonChooseFolder_Click(object sender, EventArgs e)
        {
            //DialogResult res = folderBrowserDialogMain.ShowDialog();

            //if(res == DialogResult.OK)
            //{
            //    textBoxDirectory.Text = folderBrowserDialogMain.SelectedPath;
            //}

            // Prepare a dummy string, thos would appear in the dialog
            string dummyFileName = "Save Here";

            SaveFileDialog sf = new SaveFileDialog();
            // Feed the dummy name to the save dialog
            sf.FileName = dummyFileName;

            if (sf.ShowDialog() == DialogResult.OK)
            {
                // Now here's our save folder
                textBoxDirectory.Text = Path.GetDirectoryName(sf.FileName);
                // Do whatever
            }
        }

        private void buttonRemoveReadonly_Click(object sender, EventArgs e)
        {
            try
            {
                DirectoryInfo dir = new DirectoryInfo(textBoxDirectory.Text);

                if (dir.Exists)
                {
                    w.WorkerReportsProgress = true;
                    w.WorkerSupportsCancellation = false;
                    w.DoWork += DoWork;
                    w.ProgressChanged += Report;


                    DoWorkEventArgs args = new DoWorkEventArgs(dir);
                    DoWork(this, args);
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(@"No directory selected.");
            }

        }

        private void DoWork(object sender, DoWorkEventArgs args)
        {
            DirectoryInfo dir = args.Argument as DirectoryInfo;
            int currCount = 0;
            string[] allFiles = Directory.GetFiles(dir.FullName, @"*", SearchOption.AllDirectories);
            int totalCount = allFiles.Length;

            foreach (string s in Directory.EnumerateFiles(dir.FullName, @"*", SearchOption.AllDirectories))
            {
                currCount++;
                FileInfo f = new FileInfo(s);
                f.IsReadOnly = false;                
                w.ReportProgress(currCount, @"File: " + currCount.ToString() + @"/" + totalCount.ToString() + @": " + f.FullName);

            }

            w.ReportProgress(currCount, @"Removed readonly attribute on " + totalCount + @" files.");
        }

        private void Report(object sender, ProgressChangedEventArgs e)
        {
            this.Text = e.UserState.ToString();
        }
    }
}
